import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"

const useStyles = makeStyles({
    root: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        backgroundColor: 'rgba(0, 0, 0, 0.3)'
    }
})

export default function Preloader() {
    const classes = useStyles()

    return (
        <Grid container justify='center' alignItems='center' className={classes.root}>
            <Typography variant="h1" component="h2" gutterBottom>
                Loading...
            </Typography>
        </Grid>
    )
}
