import React from 'react'
import {Grid} from "@material-ui/core"
import BookCard from "./book-card"
import {useSelector} from "react-redux"
import {booksSelector} from "../store/selectors"

export default function Catalog() {
    const books = useSelector(booksSelector)
    return (
        <Grid container spacing={2}>
            {
                books.map(i => <BookCard author={i.author} image={i.image} title={i.title}/>)
            }
        </Grid>
    )
}

