import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Grid from "@material-ui/core/Grid"

const useStyles = makeStyles(theme => ({
    media: {
        height: '70vw',
        [theme.breakpoints.up('sm')]: {
            height: '50vw'
        },
        [theme.breakpoints.up('md')]: {
            height: '37vw'
        },
        [theme.breakpoints.up('lg')]: {
            height: '24vw'
        },
    },
    title: {
        //TODO открывашка для текста что не влез
        height: '70px',
        overflow: 'hidden'
    }
}))

export default function BookCard({author, title, image}) {
    const classes = useStyles()

    return (
        <Grid xs={6} sm={4} md={3} lg={2} item>
            <Card>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={image}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h7" component="h2" className={classes.title}>
                            {title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {author}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    )
}
