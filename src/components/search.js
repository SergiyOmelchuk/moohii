import React, {useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import {useDispatch} from "react-redux"
import {getBooksThunk} from "../store/action"
import {Grid} from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        margin: '20px auto 30px'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    }
}))

export default function Search() {
    const classes = useStyles()
    const [searchText, setSearchText] = useState('')
    const dispatch = useDispatch()

    const changeSearchText = (event) => {
        setSearchText(event.target.value)
    }
    const handleClick = (e) => {
        e.preventDefault()
        dispatch(getBooksThunk(searchText))
    }
    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            dispatch(getBooksThunk(searchText))
        }
    }

    return (
        <Grid container justify='center' className='search-wrapper'>
            <Grid item xs={9} sm={6} md={3}>
                <Paper component="form" className={classes.root}>
                    <InputBase
                        className={classes.input}
                        placeholder="Search books"
                        inputProps={{'aria-label': 'search books'}}
                        onChange={changeSearchText}
                        onKeyPress={handleKeyPress}
                    />
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon onClick={handleClick}/>
                    </IconButton>
                </Paper>
            </Grid>
        </Grid>

    )
}
