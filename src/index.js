import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import {applyMiddleware, createStore} from 'redux'
import {bookReducer} from './store/book-reducer'
import {Provider} from "react-redux"
import {composeWithDevTools} from "redux-devtools-extension"
import thunk from 'redux-thunk'

const store = createStore(bookReducer,
    composeWithDevTools(applyMiddleware(thunk))
)

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
)

