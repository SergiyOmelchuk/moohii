import axios from "axios";

export function addBooks(newBooks) {
    return {
        type: 'ADD_BOOKS',
        payload: {
            books: newBooks
        }
    }
}

export function getBooksThunk(textSearch) {
    const api = `https://cors-anywhere.herokuapp.com/https://www.goodreads.com/search/index.xml?key=StrrzWNNr8JUv2mtzl7CA&q=`

    return (dispatch) => {
        axios.get(api + textSearch)
            .then(res => res.data)
            .catch(err => console.log(err.message))
            .then(data => {
                let parser = new DOMParser()
                let xml = parser.parseFromString(data, 'text/xml')
                const books = []
                xml.querySelectorAll('work').forEach(i => {
                    const book = {
                        title: i.querySelector('title').innerHTML,
                        image: i.querySelector('image_url').innerHTML,
                        smallImage: i.querySelector('small_image_url').innerHTML,
                        author: i.querySelector('name').innerHTML,
                    }
                    books.push(book)
                })
                dispatch(addBooks(books))
            })

    }
}
