export function bookReducer(state = { books: [] }, action) {
    switch (action.type) {
        case 'ADD_BOOKS': {
            return action.payload
        }
        default: {
            return state
        }
    }
}
