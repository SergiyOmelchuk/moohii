import React, {useState} from 'react'
import './App.css'
import Search from "./components/search"
import Catalog from "./components/catalog"
import Preloader from "./components/preloader"
import {makeStyles} from "@material-ui/core/styles"


const useStyles = makeStyles({
    root: {
        padding: '0 5px'
    }
})

function App() {
    const classes = useStyles()
    const [isLoading, setIsLoading] = useState(false)

    return (
        <div className={classes.root}>
            <Search/>
            <Catalog/>
            { isLoading && <Preloader/> }
        </div>
    );
}

export default App;
